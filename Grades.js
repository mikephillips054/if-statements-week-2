let finalGrade = 0 
let grade = ""

finalMark = Number(prompt("Enter the final test score to display your grade:"))

if (finalMark > 100 || finalMark < 0) { 
    grade = "Error - please enter valid score"
} else if (finalMark >= 90) { 
    grade = "A+"
} else if (finalMark >= 80) { 
    grade = "A"
} else if (finalMark >= 70) { 
    grade = "B+"
} else if (finalMark >= 60) { 
    grade = "B"
} else if (finalMark >= 50) { 
    grade = "C"
} else if (finalMark >= 40) { 
    grade = "D"
} else if (finalMark >= 0) { 
    grade = "F"
} 

console.log(`Final Marks: ${finalMark}  Grade: ${grade}`)