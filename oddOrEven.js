let inputNum = 0
let result = ""

inputNum = prompt("Enter an integer to check if it is odd or even:")


if (inputNum % 2 === 0) { 
    result = "even number"
} 
else {
    result = "odd number"
} 

console.log(`Your number ${inputNum} is ${result}.`)