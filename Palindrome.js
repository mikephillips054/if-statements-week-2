

let string1 = ""
let string2 = ""
let string3 = ""

string1 = prompt("Palindrome - three letter word.\nEnter your first letter:").toLowerCase()
string2 = prompt("Palindrome - three letter word.\nEnter your second letter:").toLowerCase()
string3 = prompt("Palindrome - three letter word.\nEnter your third letter:").toLowerCase()


//Using "if"
if (string2 !== "a" && string2 !== "e" && string2 !== "i" && string2 !== "o" && string2 !== "u") { 
    console.log("This is not an actual word!")
} 
else {
    if (string1 === string3){
        console.log("Well done, your word is a Palindrome")
    }
    else {
        console.log("This word is NOT a Palindrome!")
    }

}


//console.log(`First Letter: ${string1}  Second Letter: ${string2}  Third Letter: ${string3}`)